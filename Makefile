PREFIX = .
LIBDIR = $(PREFIX)/lib
BINDIR = $(PREFIX)/bin

all: lib tools

tools: dbcli xci2nsp

.PHONY: clean lib

clean:
	cd src/lib && $(MAKE) clean
	cd src/tools/cli && $(MAKE) clean
	cd src/tools/xci2nsp && $(MAKE) clean
	rm -rf $(LIBDIR) $(BINDIR)
	
lib:
	cd src/lib && $(MAKE)
	mkdir -p $(LIBDIR)
	cp -u src/lib/mbedtls/library/libmbedcrypto.a src/lib/libdedbae.a $(LIBDIR)

dbcli: lib
	cd src/tools/cli && $(MAKE)
	mkdir -p $(BINDIR)
	cp -u src/tools/cli/dbcli$(BIN_SUFFIX) $(BINDIR)

xci2nsp: lib
	cd src/tools/xci2nsp && $(MAKE)
	mkdir -p $(BINDIR)
	cp -u src/tools/xci2nsp/xci2nsp$(BIN_SUFFIX) $(BINDIR)
