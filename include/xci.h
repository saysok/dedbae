#pragma once

#include <stdint.h>

enum { 
	// Yes, they are this arbitrary. I don't get it either.
	XCI_GAMECARD_SIZE_1GB = 0xFA,
	XCI_GAMECARD_SIZE_2GB = 0xF8,
	XCI_GAMECARD_SIZE_4GB = 0xF0,
	XCI_GAMECARD_SIZE_8GB = 0xE0,
	XCI_GAMECARD_SIZE_16GB = 0xE1,
	XCI_GAMECARD_SIZE_32GB = 0xE2,
};

// Converts the header gamecard size flag to the gamecard size in gigabytes.
// 0 on error.
uint_fast8_t xci_gamecard_size_to_int(uint8_t flag);

#define XCI_GAMECARD_FLAG_AUTOBOOT 0x1
#define XCI_GAMECARD_FLAG_HISTORY_ERASE 0x2

typedef struct __attribute__((packed)) {
	/* 0x  0 */ char signature[0x100];
	/* 0x100 */ char magic[4]; // "HEAD"
	/* 0x104 */ uint32_t secure_area_start; // in media units (multiply by 0x200)
	/* 0x108 */ uint32_t backup_area_start; // Always 0xFFFFFFFF?
	/* 0x10C */ char titlekek_index; // twice? switchbrew lists it as being two nibbles but is ambiguous on what each one is
	/* 0x10D */ char cardsize;
	/* 0x10E */ uint8_t header_version;
	/* 0x10F */ char flags;
	/* 0x110 */ uint64_t packageid;
	/* 0x118 */ uint64_t valid_data_end; // Media units (multiply by 0x200)
	/* 0x120 */ char info_iv[0x10]; // reversed?
	/* 0x130 */ uint64_t hfs0_offset;
	/* 0x138 */ uint64_t hfs0_headersize;
	/* 0x140 */ char hfs0_headerhash[0x20];
	/* 0x160 */ char initialdata_hash[0x20];
	/* 0x180 */ uint32_t securemode_flag; // always 1?
	/* 0x184 */ uint32_t titlekey_flag; // always 2?
	/* 0x188 */ uint32_t key_flag; // always 0?
	/* 0x18C */ uint32_t normalarea_end; // Media units (multiply by 0x200)
	/* 0x190 */ char gamecard_info[0x70]; // Encrypted!
} xci_header_t;

enum {
	XCI_GAMECARD_FIRMWARE_OLD = 0x1,
	XCI_GAMECARD_FIRMWARE_WITHLOGO = 0x2,
};

enum {
	XCI_GAMECARD_SPEED_25MHZ = 0xA10010,
	XCI_GAMECARD_SPEED_50MHZ = 0xA10011,
};

typedef struct __attribute__((packed)) {
	/* 0x 0 */ uint64_t firmware_version;
	/* 0x 8 */ uint32_t speed_flag;
	/* 0x C */ uint32_t read_wait1; // Always 0x1388?
	/* 0x10 */ uint32_t read_wait2; // Always 0?
	/* 0x14 */ uint32_t write_wait1; // Always 0?
	/* 0x18 */ uint32_t write_wait2; // Always 0?
	/* 0x1C */ uint32_t firmware_mode;
	/* 0x20 */ uint32_t cup_version;
	/* 0x24 */ char empty_0[4];
	/* 0x28 */ char update_hash[8]; // What kind of hash is this exactly?
	/* 0x30 */ uint64_t cup_id; // always 0x100000000000816
	/* 0x38 */ char empty_1[0x38];
} xci_gamecard_info_t;

// At offset 0x7000 in file. UNIQUE TO EACH SPECIFIC CART -- DO NOT SHARE
typedef struct __attribute__((packed)) {
	/* 0x  0 */ char signature[0x100];
	/* 0x100 */ char magic[4]; // "CERT"
	/* 0x110 */ char unknown_0[0x1];
	/* 0x120 */ char unknown_1[0xA];
	/* 0x12A */ char unknown_2[0xD6]; // Encrypted?
} xci_gamecard_certificate_t;

typedef struct __attribute__((packed)) {
	/* 0x 0 */ uint64_t packageid; // From header
	/* 0x 8 */ char empty[8];
	/* 0x10 */ char cr_auth_data[0x10];
	/* 0x20 */ char cr_auth_mac[0x10];
	/* 0x30 */ char cr_auth_nonce[0xc];
	/* 0x3C */ char reserved[0x1C4]; // Must be empty
} xci_initial_data_t;

// Notably, none of the "meat" of the XCI has any encryption beyond what NCAs already have.
// At 0xF000 there will be an HFS0 filesystem (see hfs0.h) containing 3-4 more HFS0 filesystems, often referred to as "partitions":
// * A "normal" partition containing a copy of the CNMT and control NCAs
// * An "update" partition containing a system update
// * A "secure" partition containing all the game NCAs, and optionally a set of update NCAs+cert+tik+XML
// * Newer carts will also have a "logo" partition containing a copy of the Nintendo and Switch logos for the "launching game" loading screen
