#pragma once

typedef struct {
	char titlekeks[5][16];
	char header[32];
	char application_key_area[5][16];
} switchkeys_t;

extern switchkeys_t switchkeys;

// Parse a 16-byte key from an octet string.
void switchkeys_parse_titlekey(char* out, char* key);

// Return the default keyfile path.
// NULL on error (Usually an issue with realpath() )
char* switchkeys_default_keyfile_path();

// Import keys from the keyfile. You can specify a filename to try first; if not specified, or the specified name
// is not found, it will try some standard/logical paths.
void switchkeys_load_keyfile(char* specified_keyfile);
