#pragma once

#include <stdint.h>

// This does not correspond to any Nintendo format; it's a special struct for the library itself to return information
// about the contents of a PFS0, HFS0, or RomFS.

typedef struct {
	char* filename;
	uint64_t offset; // Offset passed to the generating function will automatically be included here
	uint64_t size;
} bae_file_entry_t;

// Should be self-explanatory. You cannot call free() directly on a bae_file_list_t.
// ALL file entries in the list will be invalidated.
void bae_free_file_list(bae_file_entry_t* list, uint_fast32_t count);
