#include "utils.h"

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "sha.h"

#define BYTES_PER_CYCLE 0x400000 // 4MiB

void hash_file_256(char* hash, baecb_read in, void* cbdata, uint_fast64_t filesize) {
	sha_ctx_t *sha_ctx = new_sha_ctx(HASH_TYPE_SHA256, 0);
	assert(sha_ctx != NULL);
	char* buf = malloc(BYTES_PER_CYCLE);
	assert(buf != NULL);
	uint_fast64_t file_pos = 0;
	uint_fast64_t readsize;

	while(file_pos + BYTES_PER_CYCLE < filesize) {
		readsize = (*in)(buf, file_pos, BYTES_PER_CYCLE, cbdata);
		sha_update(sha_ctx, buf, readsize);
		file_pos += readsize;
	}
	// Last cycle
	readsize = (*in)(buf, file_pos, filesize - file_pos, cbdata);
	sha_update(sha_ctx, buf, readsize);
	
	sha_get_hash(sha_ctx, hash);
	free(buf);
	free_sha_ctx(sha_ctx);
}

void hash_buf_256(void* buf, char* hash, uint_fast64_t buf_size) {
	sha_ctx_t *sha_ctx = new_sha_ctx(HASH_TYPE_SHA256, 0);
	assert(sha_ctx != NULL);
	sha_update(sha_ctx, buf, buf_size);
	sha_get_hash(sha_ctx, hash);
	free_sha_ctx(sha_ctx);
}
