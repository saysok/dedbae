#include "cnmt.h"

#include "utils.h"
#include "nca.h"
#include "types.h"

#include <string.h>
#include <stdio.h>
#include <assert.h>

void cnmt_store_minversion(cnmt_header_t* header, uint_fast8_t major, uint_fast8_t minor, uint_fast8_t micro, uint_fast16_t bugfix) {
	header->app.min_sysversion = major << 26 | minor << 20 | micro << 16 | bugfix;
}

void cnmt_split_minversion(cnmt_header_t* header, uint_fast8_t *major, uint_fast8_t *minor, uint_fast8_t* micro, uint_fast16_t* bugfix) {
	// For all types, the minimum version is at the same offset.
	uint32_t version = header->app.min_sysversion;
	*major =  ( version & 0xfc000000 ) >> 0x1a;
	*minor =  ( version &  0x3f00000 ) >> 0x14;
	*micro =  ( version &    0xf0000 ) >> 0x10;
	*bugfix = ( version &     0xffff );
}

char* cnmt_minversion_to_string(cnmt_header_t* header) {
	// major and minor are 6 bits so they cannot be higher than 63
	// micro is 4 bits so it cannot be higher than 15
	// bugfix is 16 bits so it cannot be higher than 65535
	// 63.63.15-65535 + trailing null == 15 characters
	const uint_fast8_t VERSION_STRING_MAX = 15;
	char version[VERSION_STRING_MAX];
	uint_fast8_t major, minor, micro;
	uint_fast16_t bugfix;
	
	cnmt_split_minversion(header, &major, &minor, &micro, &bugfix);
	snprintf(version, VERSION_STRING_MAX, "%u.%u.%u-%lu", major, minor, micro, bugfix);
	
	char* ret = strdup(version);
	assert(ret != NULL);
	return ret;
}

char* cnmt_content_type_to_string(uint8_t type) {
	switch( type ) {
		case CNMT_CONTENT_TYPE_META:
			return "Meta"; break;
		case CNMT_CONTENT_TYPE_PROGRAM:
			return "Program"; break;
		case CNMT_CONTENT_TYPE_DATA:
			return "Data"; break;
		case CNMT_CONTENT_TYPE_CONTROL:
			return "Control"; break;
		case CNMT_CONTENT_TYPE_MANUAL:
			return "Manual"; break;
		case CNMT_CONTENT_TYPE_LEGALINFO:
			return "Legalinfo"; break;
		case CNMT_CONTENT_TYPE_GAME_UPDATE:
			return "Game update"; break;
		default:
			return "INVALID";
	}
}

uint8_t cnmt_content_type_from_nca(uint8_t flag) {
	switch(flag) {
		case NCA_CONTENT_TYPE_PROGRAM:
			return CNMT_CONTENT_TYPE_PROGRAM;
		case NCA_CONTENT_TYPE_META:
			return CNMT_CONTENT_TYPE_META;
		case NCA_CONTENT_TYPE_CONTROL:
			return CNMT_CONTENT_TYPE_CONTROL;
		case NCA_CONTENT_TYPE_MANUAL:
			return CNMT_CONTENT_TYPE_LEGALINFO; // Usually the case, weirdly enough
		case NCA_CONTENT_TYPE_DATA:
			return CNMT_CONTENT_TYPE_DATA;
	}
}

void cnmt_init_header(cnmt_header_t* hd, cnmt_header_t* old_hd) {
	memset(hd, 0, sizeof(cnmt_header_t));
	
	if(old_hd != NULL) {
		hd->titleid = old_hd->titleid;
		hd->version = old_hd->version;
		hd->title_type = old_hd->title_type;
		
		if(old_hd->table_offset >= 0x10) // There is a titletype-specific header
			// Doesn't matter which; they're all the same size
			memcpy(&hd->app, &old_hd->app, sizeof(cnmt_application_header_t));
		
	}
	else
		hd->title_type = TITLE_TYPE_APPLICATION;
	
	hd->table_offset = 0x10;
}

void cnmt_create_content_record(cnmt_content_record_t* out, baecb_read in, void* cbdata, uint_fast64_t filesize) {
	char enc_hdr[ sizeof(nca_header_t) ];
	nca_header_t nca_header;
	(*in)(enc_hdr, 0, sizeof(nca_header_t), cbdata);
	nca_decrypt_header(&nca_header, enc_hdr);
	
	// Hash and NCAID
	hash_file_256(out->hash, in, cbdata, filesize);
	memcpy(out->ncaid, out->hash, 16);
	// File size
	cnmt_write_contentsize(nca_header.filesize, out);
	// Type
	out->type = cnmt_content_type_from_nca(nca_header.content_type);
	// Initialize padding, for checksum consistency
	out->padding = 0x00;
}
