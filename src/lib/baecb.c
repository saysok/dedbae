#include "baecb.h"

#include <string.h>

uint_fast64_t baecb_read_memcpy(void* buf, uint_fast64_t offset, uint_fast64_t size, void* data) {
	char* inbuf = data;
	memcpy(buf, inbuf + offset, size);
	return size;
}

uint_fast64_t baecb_write_memcpy(void* buf, uint_fast64_t offset, uint_fast64_t size, void* data) {
	char* inbuf = data;
	memcpy(inbuf + offset, buf, size);
	return size;
}

#ifndef DEDBAE_NO_STDIO_CALLBACKS
#define _FILE_OFFSET_BITS 64
#include <stdio.h>

uint_fast64_t baecb_read_stdio(void* buf, uint_fast64_t offset, uint_fast64_t size, void* data) {
	FILE* fd = data;
	fseeko(fd, offset, SEEK_SET);
	return fread(buf, 1, size, fd);
}

uint_fast64_t baecb_write_stdio(void* buf, uint_fast64_t offset, uint_fast64_t size, void* data) {
	FILE* fd = data;
	fseeko(fd, offset, SEEK_SET);
	return fwrite(buf, 1, size, fd);
}
#endif
