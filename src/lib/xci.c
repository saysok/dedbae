#include "xci.h"

uint_fast8_t xci_gamecard_size_to_int(uint8_t flag) {
	switch(flag) {
		case XCI_GAMECARD_SIZE_1GB:
			return 1;
		case XCI_GAMECARD_SIZE_2GB:
			return 2;
		case XCI_GAMECARD_SIZE_4GB:
			return 4;
		case XCI_GAMECARD_SIZE_8GB:
			return 8;
		case XCI_GAMECARD_SIZE_16GB:
			return 16;
		case XCI_GAMECARD_SIZE_32GB:
			return 32;
		default:
			return 0;
	}
}
