#pragma once

#include "baecb.h"

#include <stdint.h>

// Make sha256 hash out of file
void hash_file_256(char* hash, baecb_read in, void* cbdata, uint_fast64_t filesize);
// Make sha256 hash out of buffer
void hash_buf_256(void* buf, char* hash, uint_fast64_t buf_size);
