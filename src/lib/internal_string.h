#pragma once

#include <string.h>
#include <errno.h>

static inline char *strndup(const char *s, size_t n) {
	size_t real_n;
	for(real_n=0; real_n<n; real_n++)
		if(s[real_n] == '\0') break;

	char *ret = malloc(real_n+1);
	if(ret == NULL) {
		errno = ENOMEM;
		return NULL;
	}
	ret[real_n] = '\0';
	
	memcpy(ret, s, real_n);
	return ret;
}
/*
static inline char *strdup(const char *s) {
	return strndup(s, strlen(s));
}
*/
