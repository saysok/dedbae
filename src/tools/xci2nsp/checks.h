#pragma once

#define _FILE_OFFSET_BITS 64
#include <stdio.h>
#include <stdbool.h>

void check(bool condition, const char* message, ...);

void* checked_malloc(size_t size);

size_t checked_fread(void *ptr, size_t size, size_t nmemb, FILE *stream);

size_t checked_fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream);

int checked_fseeko(FILE *stream, off_t offset, int whence);

off_t checked_ftello(FILE *stream);

void check_key(char* key, char* keyname);
