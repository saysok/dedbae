#include "actions.h"

#include "xci.h"
#include "hfs0.h"

#define _FILE_OFFSET_BITS 64
#include <stdio.h>
#include <stdlib.h>

void ls_files(bae_file_entry_t* files, int_fast64_t num_files) {

	printf("Number of files : %ld\n", num_files);
	
	for(int_fast64_t i=0; i<num_files; i++) {
		printf("    %s:\n", files[i].filename );
		
		printf("            File offset: %lx\n", files[i].offset);
		printf("            Size       : %lu bytes\n", files[i].size);
		printf("\n");
	}
}

void act_xciinfo(char* file) {
	xci_header_t header;
	
	FILE* fd = fopen(file, "rb");
	
	printf("XCI Header:\n");
	fread(&header, 1, sizeof(xci_header_t), fd);
	
	printf("Secure area starts at: 0x%llx\n", header.secure_area_start * 0x200ULL);
	printf("Titlekek index       : %u\n", header.titlekek_index);
	printf("Gamecard size        : %u GB\n", xci_gamecard_size_to_int(header.cardsize));
	printf("XCI header version   : %u\n", header.header_version);
	
	printf("Autoboot             : ");
	if(header.flags & XCI_GAMECARD_FLAG_AUTOBOOT)
		printf("yes");
	else
		printf("no");
	printf("\n");
	
	printf("History Erase        : ");
	if(header.flags & XCI_GAMECARD_FLAG_HISTORY_ERASE)
		printf("yes");
	else
		printf("no");
	printf("\n");
	
	printf("Package ID           : 0x%016lx\n", header.packageid);
	printf("Valid data ends at   : 0x%llx\n", header.valid_data_end * 0x200ULL);
	printf("HFS0 offset          : 0x%lx\n", header.hfs0_offset);
	printf("HFS0 header size     : 0x%lx\n", header.hfs0_headersize);
	printf("\n");

	// TODO: Gamecard info, and *maybe* certificate?
	
	printf("Master HFS0:\n");
	
	bae_file_entry_t* partitions;
	int_fast64_t num_partitions = hfs0_parse(&partitions, &baecb_read_stdio, fd, header.hfs0_offset);
	
	ls_files(partitions, num_partitions);
	
	for(uint_fast8_t i=0; i<num_partitions; i++) {
		printf( "%s: ", partitions[i].filename);
		
		bae_file_entry_t* files;
		int_fast64_t num_files = hfs0_parse(&files, &baecb_read_stdio, fd, partitions[i].offset);
		
		ls_files(files, num_files);
		
		bae_free_file_list(files, num_files);
	}
	
	bae_free_file_list(partitions, num_partitions);
	
	fclose(fd);
}
