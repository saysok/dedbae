#pragma once

#include <stdint.h>

void act_recrypt(char* infilename, char* outfilename); // recrypt.c
void act_create(char* nca_filename, char* section_filenames[], uint_fast8_t num_sections); // create.c
// TODO
void act_gencnmt(char* outfilenames, char* nca_filenames[], uint_fast16_t num_files); // gencnmt.c
void act_printcnmt(char* file); // printcnmt.c
void act_xciinfo(char* file); // xciinfo.c
void act_extractxci(char* file); // extractxci.c
void act_printromfs(char* file); // printromfs.c
