#include "actions.h"

#include "args.h"
#include "nca.h"
#include "pfs0.h"
#include "utils.h"

#include <string.h>
#define _FILE_OFFSET_BITS 64
#include <stdio.h>
#include <stdlib.h>

void act_create(char* nca_filename, char* section_filenames[], uint_fast8_t num_sections) {
	nca_header_t header;
	
	if(cmdline_args.oldfile != NULL) {
		FILE* old_fd = fopen(cmdline_args.oldfile, "rb");
		
		char old_enc_header[ sizeof(nca_header_t) ];
		nca_header_t old_header;
		fread(old_enc_header, 1, sizeof(nca_header_t), old_fd);
		fclose(old_fd);
		
		nca_decrypt_header(&old_header, old_enc_header);
		
		nca_init_header(&header, &old_header);
	}
	else
		nca_init_header(&header, NULL);
	
	if(cmdline_args.location != NULL) {
		if( strcmp(cmdline_args.location, "console") == 0 )
			header.location = NCA_LOCATION_CONSOLE;
		else
			header.location = NCA_LOCATION_GAMECARD;
	}
	
	if(cmdline_args.content != NULL) {
		if( strcmp(cmdline_args.content, "program") == 0 )
			header.content_type = NCA_CONTENT_TYPE_PROGRAM;
		else if( strcmp(cmdline_args.content, "meta") == 0 )
			header.content_type = NCA_CONTENT_TYPE_META;
		else if( strcmp(cmdline_args.content, "control") == 0 )
			header.content_type = NCA_CONTENT_TYPE_CONTROL;
		else if( strcmp(cmdline_args.content, "manual") == 0 )
			header.content_type = NCA_CONTENT_TYPE_MANUAL;
		else // strcmp(cmdline_args.content, "data") == 0
			header.content_type = NCA_CONTENT_TYPE_DATA;
	}
	
	if(cmdline_args.titleid != NULL) {
		header.titleid = strtoull(cmdline_args.titleid, NULL, 16);
	}
	
	if(cmdline_args.out_titlekey != NULL) {
		// In all observed official cases, the Rights ID is the title ID padded with zeroes,
		// with the value of the NCA crypto_type2 field appended.
		// TitleID is stored as an LSB integer, but RightsID is a byte array
		char* titleid_bytes = (char*) &header.titleid;
		for(int i=0; i<8; i++) {
			int j=7-i;
			header.rights_id[i] = titleid_bytes[j];
		}
		header.rights_id[15] = header.crypto_type2;
	}
	
	for(uint_fast8_t i=0; i < num_sections; i++)
		if(cmdline_args.crypto[i] != 0)
			header.section_headers[i].cryptotype = cmdline_args.crypto[i];
	
	FILE* nca = fopen(nca_filename, "wb");
	
	for(uint_fast8_t i=0; i<num_sections; i++) {
		FILE* section = fopen(section_filenames[i], "rb");

		if(i==0)
			header.section_table[i].media_offset = 6; // End of NCA header
		else
			header.section_table[i].media_offset = header.section_table[i-1].media_end;
		
		fseeko(section, 0, SEEK_END);
		uint64_t section_size = ftello(section);
		fseeko(section, 0, SEEK_SET);
		
		pfs0_header_t pfs_hd;
		fread(&pfs_hd, 1, sizeof(pfs0_header_t), section);
		if( strncmp(pfs_hd.magic, "PFS0", 4) == 0 )
			nca_inject_pfs0(&header, i,
					&baecb_write_stdio, nca,
					&baecb_read_stdio, section, section_size, cmdline_args.out_titlekey);
		else
			nca_inject_romfs(&header, i,
					&baecb_write_stdio, nca,
					&baecb_read_stdio, section, section_size, cmdline_args.out_titlekey);
		
		fclose(section);
	}
	
	fseeko(nca, 0, SEEK_END);
	header.filesize = ftello(nca);
	
	char enc_hdr[ sizeof(nca_header_t) ];
	nca_encrypt_header(enc_hdr, &header);
	fseeko(nca, 0, SEEK_SET);
	fwrite(enc_hdr, 1, sizeof(nca_header_t), nca);
	fclose(nca);
}
