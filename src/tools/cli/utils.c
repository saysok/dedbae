#include "utils.h"

#define _FILE_OFFSET_BITS 64
#include <stdio.h>
#include <stdlib.h>

void bail(char* message) {
	printf("%s\n\n", message);
	exit(1);
}
